nis (4.0) unstable; urgency=medium

  Starting from this release, the `nis` package has been re-organized and
  split in three different packages: ypserv, ypbind-mt and yp-tools.
  The whole debconf configuration - that was limited to configure a NIS
  client in simple environments - has been removed.

  NIS services are now totally neither configured nor started after new
  installations.

  A simple post-installation script tries to migrate existing installations
  at upgrade time, assuming that all pieces are correct and in the right
  places. Admins of existing NIS setup should maintain their current
  /etc/default/nis configuration file and the NIS will be setup
  via systemd. If admins install the maintainer's version instead,
  clients and servers service will NOT be activated, but all NIS 
  files will be maintained in place.

  The administrators must refer to /usr/share/doc/nis/nis.debian.howto
  documentation to configure correctly both new clients and servers,
  using the available systemd units.
  If the administrator still uses sysv init files, courtesy scripts are
  even provided, but the old /etc/init.d/nis all-in-one script has gone.

  Finally, ypbind does not start in broadcast mode automagically anymore,
  if you need broadcasting use the -broadcast command line option or add
  broadcast to the /etc/yp.conf file. This is totally up to you, but note
  that broadcast mode is _not_ a sane setup. 
  
  For security reasons, you are also warmly invited to use MIT Kerberos 
  for passwords, instead of NIS shadow passwords that allow reading of 
  password hashes to any NIS server and client.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Fri, 08 Jan 2021 13:10:26 +0100
